﻿/*
 *  date: 2018-07-15
 *  author: John-chen
 *  cn: UI枚举
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 窗口类型
    /// </summary>
    public enum WindowType
    {
        FullScreen,                 // 全屏UI
        SubWindow,                  // 二级节点(非全屏)
        PopWindow,                  // 弹出窗口
        TipWindow,                  // 提示窗口
        GuideWindow,                // 新手指引
    }

    /// <summary>
    /// 窗口缓存时间
    /// </summary>
    public enum WindowCacheTime
    {
        Cache0 = 0,                     // 立马销毁
        Cache1 = 1,                     // 缓存1分钟
        Cache5 = 5,                     // 缓存5分钟
        Cache10 = 10,                   // 缓存10分钟
        Cache30 = 30,                   // 缓存30分钟
        Cache60 = 60,                   // 缓存60分钟
        CacheMax = 100                  // 永远不销毁
    }

    /// <summary>
    /// 窗口的状态
    /// </summary>
    public enum WindowState
    {
        Default,
        Created,
        InShow,
        InHide,
        InDestroy,
    }

    /// <summary>
    /// 基础UI框架
    /// window的信息,通过装饰模式实现每个window特殊定制
    /// </summary>
    public class WindowInfo
    {
        /// <summary>
        /// 窗口名字
        /// </summary>
        public string Name { get { return _name;} }

        /// <summary>
        /// 地址
        /// </summary>
        public string Path { get { return _path; } }

        /// <summary>
        /// 窗口类型
        /// </summary>
        public WindowType WndType { get { return _type;} }

        /// <summary>
        /// 窗口缓存时间
        /// </summary>
        public WindowCacheTime CacheTime { get { return  _cacheTime;} }

        /// <summary>
        /// 窗口状态
        /// </summary>
        public WindowState WinState
        {
            get { return _winState; }
            set { _winState = value; }
        }

        /// <summary>
        /// 创建本实例的时间
        /// </summary>
        public float CreateTime { get { return _createTime;} }

        /// <summary>
        /// 加载本窗口资源的时间
        /// </summary>
        public float LoadedTime { get { return _loadedTime;} }

        /// <summary>
        /// 本窗口的刷新时间
        /// </summary>
        public float RefreshTime { get { return _refreshTime;} }

        public WindowInfo(WindowType type, WindowCacheTime time, string path, string name = "")
        {
            _name = name;
            _type = type;
            _path = path;
            _cacheTime = time;
            _createTime = Time.realtimeSinceStartup;
        }

        /// <summary>
        /// 设置加载时间
        /// </summary>
        public void UpdateLoadedTime()
        {
            _loadedTime = Time.realtimeSinceStartup;

            UpdateTime();
        }

        /// <summary>
        /// 更新本窗口
        /// </summary>
        public void UpdateTime()
        {
            _refreshTime = Time.realtimeSinceStartup;
        }

        protected string _name;
        protected string _path;
        protected WindowType _type;
        protected float _createTime;
        protected float _loadedTime;
        protected float _refreshTime;
        protected WindowState _winState;
        protected WindowCacheTime _cacheTime;
    }
}