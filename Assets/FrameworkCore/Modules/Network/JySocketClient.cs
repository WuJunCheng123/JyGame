﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: socket
 *  en: todo:
 */

using System;

namespace JyFramework
{
    /// <summary>
    /// 客户端socket
    /// </summary>
    public class JySocketClient : BaseSocketClient
    {
        public JySocketClient(string ip, int port):base(ip, port)
        {
            _packagePool = new InfinitePool<NetPackage>();
        }

        ///// <summary>
        ///// 连接服务器
        ///// </summary>
        //public override void ConnectServer()
        //{
        //    base.ConnectServer();

        //}

        ///// <summary>
        ///// 发送信息到服务器
        ///// </summary>
        ///// <param name="msg"></param>
        //public override void SendMsg(byte[] msg)
        //{
        //    base.SendMsg(msg);

        //}

        /// <summary>
        /// 发送包
        /// </summary>
        /// <param name="package"></param>
        public void SendPackage(NetPackage package)
        {
            SendMsg(package.AllData);
        }

        /// <summary>
        /// 对服务器数据进行操作
        /// </summary>
        /// <param name="svrMsg"></param>
        protected override void HandleMsg(JySvrMsg svrMsg)
        {
            int index = 0;
            HandleStickyData(svrMsg, index);
        }


        /// <summary>
        /// 处理粘包
        /// </summary>
        /// <param name="svrMsg"> 粘包的数据 </param>
        /// <param name="index"> 处理索引 </param>
        private void HandleStickyData(JySvrMsg svrMsg, int index)
        {
            if (index == svrMsg.Len) return;

            // 1.取长度
            int msgAllLen = BitConverter.ToInt32(svrMsg.Msg, index);
            byte[] buffer = new byte[msgAllLen];
            Array.Copy(svrMsg.Msg, index, buffer, 0, msgAllLen);

            if (_packagePool.Count == 0)
            {
                NetPackage tempPg = new NetPackage(0, null);
                _packagePool.AddObject(tempPg);
            }

            NetPackage pg = _packagePool.GetObject();
            pg.SetSvrMsg(msgAllLen, buffer);
            pg.DeCode();
            var en = NetEventId.GetProtobufInsEvent((int) pg.MsgId);
            _event.NotifyEvent(en, pg);
            _packagePool.ReleaseObject(pg);

            index += msgAllLen;
            HandleStickyData(svrMsg, index);
        }

        private InfinitePool<NetPackage> _packagePool;
    }
}
