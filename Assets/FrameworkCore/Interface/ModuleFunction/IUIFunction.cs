﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: UI使用接口
 *  en: todo:
 */

namespace JyFramework
{
    public delegate void UIAction(params object[] args);

    /// <summary>
    /// UI使用接口
    /// </summary>
    public interface IUIFunction
    {
        /// <summary>
        /// 显示窗口
        /// </summary>
        /// <typeparam name="T"> 窗口类 </typeparam>
        /// <param name="action"> 加载成功后回调 </param>
        /// <param name="args"> 传入的参数 </param>
        T Show<T>(WndAction action = null) where T : BaseWindow, new();

        /// <summary>
        /// 影藏窗口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="args"></param>
        T Hide<T>(WndAction action = null) where T : BaseWindow;

        /// <summary>
        /// 获取已经被打开过未被销毁过的窗口
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetWindow<T>() where T : BaseWindow;

        /// <summary>
        /// 影藏所有窗口
        /// </summary>
        void HideAll();

        /// <summary>
        /// 清理所有UI
        /// </summary>
        void ClearAll();
    }
}